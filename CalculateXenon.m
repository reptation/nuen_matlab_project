function [arrayXenon,limitXenon,denumXenon] = CalculateXenon(infoConstants,infoIodine,infoXenon,limitIodine,denumIodine)

lambda = infoXenon(1);
sigma = infoXenon(2);
gamma = infoXenon(3);
tFlux = infoConstants(1);
xSec = infoConstants(3);



% Precalculating Values
preXenon = PreCalculations(lambda, sigma, gamma, tFlux);

% Calculate Limit
denumXenon = preXenon.fDenominator;
numXenon = gamma*xSec*tFlux + infoIodine(1)*limitIodine;
limitXenon = numXenon/denumXenon;

arrayXenon=[];

for j=0:infoConstants(2)
    iDiff = 0-limitIodine;
    xDiff = 0-limitXenon;
    pOne = xDiff+(infoIodine(1)*iDiff)/(denumIodine-denumXenon);
    expOne = exp(-denumXenon*j*3600);
    pTwo = (infoIodine(1)*iDiff)/(denumIodine-denumXenon);
    expTwo = exp(-denumIodine*j*3600);
    arrayXenon(j+1,:) = limitXenon+pOne*expOne-pTwo*expTwo;
end

end