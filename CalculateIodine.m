function [arrayIodine,limitIodine,denumIodine] = CalculateIodine(infoConstants,infoIodine)

arrayIodine=[];
reactorOff = false;

lambda = infoIodine(1);
sigma = infoIodine(2);
gamma = infoIodine(3);
xSec = infoConstants(3);

for j=0:infoConstants(2)
    if reactorOff == false
        tFlux = infoConstants(1);
        Io = 0;
        
        denumIodine = lambda+sigma*tFlux;
        numIodine = gamma*xSec*tFlux;
        limitIodine = numIodine/denumIodine;

        iDiff = Io-limitIodine;
        iExp = exp(-denumIodine*j*3600);
        arrayIodine(j+1,:) = limitIodine+iDiff*iExp; 
    elseif reactorOff == true
        tFlux = 0;
        Io = arrayIodine(80,:);
        
        ndenumIodine = lambda+sigma*tFlux;
        nnumIodine = gamma*xSec*tFlux;
        nlimitIodine = nnumIodine/ndenumIodine;

        niDiff = Io-nlimitIodine;
        niExp = exp(-ndenumIodine*j*3600);
        arrayIodine(j+1,:) = nlimitIodine+niDiff*niExp;
    end
 
    if j == 80
        reactorOff = true;
    end
end

end