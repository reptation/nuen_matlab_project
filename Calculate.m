function [arrayIodine,arrayXenon] = Calculate(infoConstants,infoIodine,infoXenon)

reactorOff = false;

arrayIodine=[];
arrayXenon=[];


iLambda = infoIodine(1);
iSigma = infoIodine(2);
iGamma = infoIodine(3);

xLambda = infoXenon(1);
xSigma = infoXenon(2);
xGamma = infoXenon(3);

xSec = infoConstants(3);

for j=0:infoConstants(2)
  
    % Reactor Key
    if j == 80
        reactorOff = true;
    end
    
    
    % Reactor On/Off
    if reactorOff == false
        tFlux = infoConstants(1);

        % -------------------------------------------------- Iodine Begin
        % Initial Condition
        I0 = 0;

        % Calculate Limit
        denumIodine = iLambda+iSigma*tFlux;
        numIodine = iGamma*xSec*tFlux;
        limitIodine = numIodine/denumIodine;
        
        % Calculate Values
        iDiff = I0-limitIodine;
        iExp = exp(-denumIodine*j*3600);
        
        % Storing Array
        arrayIodine(j+1,:) = limitIodine+iDiff*iExp; 
        % -------------------------------------------------- Iodine End
        
        
        
        % -------------------------------------------------- Xenon Begin
        % Initial Condition
        X0 = 0;
        
        % Calculate Limit
        denumXenon = xLambda+xSigma*tFlux;
        numXenon = xGamma*xSec*tFlux + iLambda*limitIodine;
        limitXenon = numXenon/denumXenon;
        
        % Calculate Values
        xDiff = X0-limitXenon;
        pOne = xDiff+(iLambda*iDiff)/(denumIodine-denumXenon);
        expOne = exp(-denumXenon*j*3600);
        pTwo = (iLambda*iDiff)/(denumIodine-denumXenon);
        expTwo = exp(-denumIodine*j*3600);

        % Storing Array
        arrayXenon(j+1,:) = limitXenon+pOne*expOne-pTwo*expTwo;
        % -------------------------------------------------- Xenon End
        
    elseif reactorOff == true
        tFlux = 0;
       
        % -------------------------------------------------- Iodine Begin
        % Initial Condition
        I0 = arrayIodine(80,:);

        % Calculate Limit
        denumIodine = iLambda+iSigma*tFlux;
        numIodine = iGamma*xSec*tFlux;
        limitIodine = numIodine/denumIodine;
        
        % Calculate Values
        iDiff = I0-limitIodine;
        iExp = exp(-denumIodine*j*3600);
        
        % Storing Array
        arrayIodine(j+1,:) = limitIodine+iDiff*iExp; 
        % -------------------------------------------------- Iodine End
        
        
        
        % -------------------------------------------------- Xenon Begin
        % Initial Condition
        X0 = arrayXenon(80,:);
        
        % Calculate Limit
        denumXenon = xLambda+xSigma*tFlux;
        numXenon = xGamma*xSec*tFlux + iLambda*limitIodine;
        limitXenon = numXenon/denumXenon;
        
        % Calculate Values
        xDiff = X0-limitXenon;
        pOne = xDiff+(iLambda*iDiff)/(denumIodine-denumXenon);
        expOne = exp(-denumXenon*j*3600);
        pTwo = (iLambda*iDiff)/(denumIodine-denumXenon);
        expTwo = exp(-denumIodine*j*3600);

        % Storing Array
        arrayXenon(j+1,:) = limitXenon+pOne*expOne-pTwo*expTwo;
        % -------------------------------------------------- Xenon End
    end
end

end