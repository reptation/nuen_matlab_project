classdef PreCalculations < handle
    properties
        lambda;
        sigma;
        gamma;
        flux;
    end
    
    properties(Dependent)
        iLimit;
        fDenominator; 
    end
    
    properties(Constant = true)
        barnUnit = 1E-24;
    end
  
    
    %{ 
===========================================================================
        Main Method
===========================================================================
    %}
    methods
        % =================================== Initilization
        function cObj = PreCalculations(lambda,sigma,gamma,flux)
            if(nargin>0)
                cObj.lambda = lambda;
                cObj.sigma = sigma;
                cObj.gamma = gamma;
                cObj.flux = flux;
            end
        end
    end
    
    
    %{ 
===========================================================================
        Precalculation Method
===========================================================================
    %}
    methods  
        % =================================== Calculate fraction denominator
        function fracDen = get.fDenominator(cObj)
            fracDen = cObj.lambda+cObj.sigma*cObj.barnUnit*cObj.flux;
        end
    end
end