%{ 
===========================================================================

File: Main

Author: Team 2

===========================================================================
%}

% -------------------------------------------------- Initializing Variables
% --------------------- Common
barnUnit = 1E-24;
% --------------------- Reactor
tFlux = 4E13;
runTime = 160;
xSec = 0.39497;
infoConstants = [tFlux,runTime,xSec];
% --------------------- Iodine
lambdaI = 2.874E-5;
sigmaI = 7*barnUnit;
gammaI = 0.0639;
infoIodine = [lambdaI,sigmaI,gammaI];
% --------------------- Xenon
lambdaX = 2.093E-5;
sigmaX = 2.65E6*barnUnit;
gammaX = 0.00237;
infoXenon = [lambdaX,sigmaX,gammaX];

% --------------------- Calculate
[arrayIodine,arrayXenon] = Calculate(infoConstants,infoIodine,infoXenon);

% --------------------- Initialize Values for Graph
x1 = linspace(0,160,length(arrayIodine));
x2 = linspace(0,160,length(arrayXenon));
y1 = arrayIodine;
y2 = arrayXenon;

% --------------------- Graph
figure
plot(x1,y1,x2,y2);
xlabel('Time(hr)');
ylabel('I/Xe');




% ------------------------------------------------------------------------- Legacy Code
% --------------------- Iodine
%[arrayIodine,limitIodine,denumIodine] = CalculateIodine(infoConstants,infoIodine);
% --------------------- Xenon
%[arrayXenon,limitXenon] = CalculateXenon(infoConstants,infoIodine,infoXenon,limitIodine,denumIodine);
%fprintf('Percent Iodine: %f \n',arrayIodine(80)/limitIodine*100)
%fprintf('Percent Xenon: %f \n',arrayXenon(80)/limitXenon*100)
% ------------------------------------------------------------------------- Legacy Code
