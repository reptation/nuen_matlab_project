# Important Notice #
The readme will be updated multiple times throughout the development process. Everything is subject to change.


# Product Poisoning #
Simple time evolution involing fission product poisoning.


##Getting Started ##
Below are the guidelines to setup the program.


### Prerequisites ###
* Latest working copy of MATLAB
* Working text editor or IDE.
* **NOTE** Preferably some sort of git installed.


### Installing ###
* Download the repo manually or using git.
* Open the repo and import into your IDE or text editor.
* Happy coding.

## Deployment ##
* Build MATLAB code.

## Built With ##
* MATLAB

## Contributing ##
* Please ensure code is clean, clear and comments are written.
* There is no dedicated coded review. Be responsible.
* If you don't know what you're doing, ask someone who does.


### Who do I talk to? ###


### License ###


### Acknowledgments ###